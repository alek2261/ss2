### AWS infrastructure and IAM ### 
1. Which of the following describes a physical location around the world where AWS clusters data centers?  
- a) Endpoint; 
- b) Collection;  
- c) Fleet; 
- d) Region – A region is a named set of AWS resources in the same geographical area. A region comprises at least two Availability Zones.
- The correct answer is d).
  
2. Each AWS region is composed of two or more locations that offer organizations the ability to operate production systems that are more highly available, fault tolerable, and scalable than would be possible using a single data center. What are those locations called? 
- a) Replication areas;  
- b) Availability zones – An Availability zone is a distinct location within a region that is insulated from failures in other AZ and provides inexpensive, low-latency network connectivity to other AZ in the same region. 
- c) Geographic districts;  
- d) Compute centers 
- The correct answer is b).

3. Which of the following are found in an IAM policy? (Choose 2 answers)  
- a) Service Name;  
- b) Region;  
- c) Action;  
- d) Password; 
- The correct answers are b) and c).

4. What is the format of an IAM policy?  
- a) XML;  
- b) Key/value pairs;  
- c) JSON;  
- d) Tab-delimited text. 
- The correct answer is c).