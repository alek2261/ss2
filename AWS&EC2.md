### Amazon Web Services and Elastic Compute Cloud ###
AWS is the world’s largest online book store also happens to run the world’s largest public cloud provider. It offers platform as a service, infrastructure as a service, 
serverless computing and much more. On this massive platform, companies are able to let their imagination go wild, think of new business models and offer a lot more services to their customers. 
EC2 is short for Elastic Compute Cloud and offers a virtual machine in the cloud. As with any virtual machine you can connect a virtual network interface to it, as well as some storage. 
What most virtual machines can’t do however is auto-scaling: growing and shrinking your fleet of servers on-demand or completely automatically. 
EC2 has the unique ability that this is all built-in to the service. Here are some relevant terms that you will come across.  
- Spot requests allow you to bit on spare Amazon EC2 computing capacity.  
- Reserved instances is a pricing model based on a commitment. 
- AMIs(Amazon Machine Images) are a lot like snapshots in VMware. You can launch new EC2 instances from an AMI and you can even share your AMI with multiple AWS accounts. 
- Launch configurations and autoscaling groups make sure your EC2 instances can scale up and down based on demand. 
Your EC2 instances will automatically be added to a load balancer you specify to make sure your applications are available 24/7. 
- Run commands give you a way to remotely execute scripts on a single EC2 instances or an entire fleet, either on-demand or on a schedule.