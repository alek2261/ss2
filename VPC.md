### Amazon Virtual Private Cloud ###
VPC allows you to build a virtualized private network inside of AWS complete with subnets, NAT gateways, VPN connections, routing tables, security groups and much more. Everything you expect to have inside of your own data center is right there in AWS. You can easily customize the network configuration for your Amazon VPC. For example, you can give an access to the internet to your web servers that are pasted in a public-facing subnet and you can divide your backend systems like databases or application servers in a private-facing subnet that has no internet access. The security of VPC is two-fold. It uses security groups as firewall to control traffic at the instance level, but it also uses a network access control lists as a firewall to control traffic at the subnet level. In Amazon VPC you can also create dedicated instances on hardware to isolate physically the dedicated instances from the non-dedicated ones and the instances owned by others.  
Here are some relevant terms:
- Security groups allow you to set inbound and outbound firewall rules on your network.
- Internet Gateway is a service that allows EC2 instances in public subnets to access the internet.
- NAT Gateways is a service that allows EC2 instances in private subnets to access the internet.
- Elastic IPs are fixed IP addresses that you can assign to an EC2 instance.
- Endpoints are used to create a direct connection to different AWS services so traffic to those services doesn’t have to pass your Internet- or NAT gateways.
- Peering connections are used to create a mesh of different VPC’s.
- Virtual Private Gateways are managed VPN gateways to create tunnels between your on-premises infrastructure and AWS.
